<?php

namespace App\Entity;

use App\Repository\ImportRunsEntriesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImportRunsEntriesRepository::class)
 */
class ImportRunsEntries
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ImportRuns::class, inversedBy="importRunsEntries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $importRun;

    /**
     * @ORM\Column(type="integer")
     */
    private $rows_expected;

    /**
     * @ORM\Column(type="integer")
     */
    private $rows_processed;

    /**
     * @ORM\Column(type="integer")
     */
    private $rows_errored;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $errors_content;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $area_code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImportRun(): ?ImportRuns
    {
        return $this->importRun;
    }

    public function setImportRun(?ImportRuns $importRun): self
    {
        $this->importRun = $importRun;

        return $this;
    }

    public function getRowsExpected(): ?int
    {
        return $this->rows_expected;
    }

    public function setRowsExpected(int $rows_expected): self
    {
        $this->rows_expected = $rows_expected;

        return $this;
    }

    public function getRowsProcessed(): ?int
    {
        return $this->rows_processed;
    }

    public function setRowsProcessed(int $rows_processed): self
    {
        $this->rows_processed = $rows_processed;

        return $this;
    }

    public function getRowsErrored(): ?int
    {
        return $this->rows_errored;
    }

    public function setRowsErrored(int $rows_errored): self
    {
        $this->rows_errored = $rows_errored;

        return $this;
    }

    public function getErrorsString(): ?string
    {
        return $this->errors_content;
    }

    public function setErrorsString(?string $errors_content): self
    {
        $this->errors_content = $errors_content;

        return $this;
    }

    public function getAreaCode(): ?string
    {
        return $this->area_code;
    }

    public function setAreaCode(string $area_code): self
    {
        $this->area_code = $area_code;

        return $this;
    }
}
