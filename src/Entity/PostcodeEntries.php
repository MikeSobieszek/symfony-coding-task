<?php

namespace App\Entity;

use App\Repository\PostcodeEntriesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostcodeEntriesRepository::class)
 */
class PostcodeEntries
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $Postcode;

    /**
     * @ORM\Column(type="integer")
     */
    private $Positional_quality_indicator;

    /**
     * @ORM\Column(type="integer")
     */
    private $Eastings;

    /**
     * @ORM\Column(type="integer")
     */
    private $Northings;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $Country_code;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $NHS_regional_HA_code;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $NHS_HA_code;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $Admin_county_code;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $Admin_district_code;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $Admin_ward_code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostcode(): ?string
    {
        return $this->Postcode;
    }

    public function setPostcode(string $Postcode): self
    {
        $this->Postcode = $Postcode;

        return $this;
    }

    public function getPositionalQualityIndicator(): ?int
    {
        return $this->Positional_quality_indicator;
    }

    public function setPositionalQualityIndicator(?int $Positional_quality_indicator): self
    {
        $this->Positional_quality_indicator = $Positional_quality_indicator;

        return $this;
    }

    public function getEastings(): ?int
    {
        return $this->Eastings;
    }

    public function setEastings(int $Eastings): self
    {
        $this->Eastings = $Eastings;

        return $this;
    }

    public function getNorthings(): ?int
    {
        return $this->Northings;
    }

    public function setNorthings(int $Northings): self
    {
        $this->Northings = $Northings;

        return $this;
    }

    public function getCountryCode(): ?string
    {
        return $this->Country_code;
    }

    public function setCountryCode(?string $Country_code): self
    {
        $this->Country_code = $Country_code;

        return $this;
    }

    public function getNHSRegionalHACode(): ?string
    {
        return $this->NHS_regional_HA_code;
    }

    public function setNHSRegionalHACode(?string $NHS_regional_HA_code): self
    {
        $this->NHS_regional_HA_code = $NHS_regional_HA_code;

        return $this;
    }

    public function getNHSHACode(): ?string
    {
        return $this->NHS_HA_code;
    }

    public function setNHSHACode(?string $NHS_HA_code): self
    {
        $this->NHS_HA_code = $NHS_HA_code;

        return $this;
    }

    public function getAdminCountyCode(): ?string
    {
        return $this->Admin_county_code;
    }

    public function setAdminCountyCode(?string $Admin_county_code): self
    {
        $this->Admin_county_code = $Admin_county_code;

        return $this;
    }

    public function getAdminDistrictCode(): ?string
    {
        return $this->Admin_district_code;
    }

    public function setAdminDistrictCode(?string $Admin_district_code): self
    {
        $this->Admin_district_code = $Admin_district_code;

        return $this;
    }

    public function getAdminWardCode(): ?string
    {
        return $this->Admin_ward_code;
    }

    public function setAdminWardCode(?string $Admin_ward_code): self
    {
        $this->Admin_ward_code = $Admin_ward_code;

        return $this;
    }
}
