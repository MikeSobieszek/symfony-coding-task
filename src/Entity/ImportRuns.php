<?php

namespace App\Entity;

use App\Repository\ImportRunsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImportRunsRepository::class)
 */
class ImportRuns
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    /**
     * @ORM\Column(type="integer")
     */
    private $errors_count;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $errors_content;

    /**
     * @ORM\OneToMany(targetEntity=ImportRunsEntries::class, mappedBy="importRun", orphanRemoval=true)
     */
    private $importRunsEntries;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $rows_expected_json;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metadata;

    public function __construct()
    {
        $this->importRunsEntries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getErrorsCount(): ?int
    {
        return $this->errors_count;
    }

    public function setErrorsCount(int $errors_count): self
    {
        $this->errors_count = $errors_count;

        return $this;
    }

    public function incrementErrorsCount(): self
    {
        $this->errors_count++;

        return $this;
    }

    public function getErrorsString(): ?string
    {
        return $this->errors_content;
    }

    public function setErrorsString(?string $errors_content): self
    {
        $this->errors_content = $errors_content.' ';

        return $this;
    }

    public function addToErrorsString(?string $errors_content): self
    {
        $this->errors_content .= $errors_content.' ';

        return $this;
    }

    /**
     * @return Collection|ImportRunsEntries[]
     */
    public function getImportRunsEntries(): Collection
    {
        return $this->importRunsEntries;
    }

    public function addImportRunsEntry(ImportRunsEntries $importRunsEntry): self
    {
        if (!$this->importRunsEntries->contains($importRunsEntry)) {
            $this->importRunsEntries[] = $importRunsEntry;
            $importRunsEntry->setImportRun($this);
        }

        return $this;
    }

    public function removeImportRunsEntry(ImportRunsEntries $importRunsEntry): self
    {
        if ($this->importRunsEntries->contains($importRunsEntry)) {
            $this->importRunsEntries->removeElement($importRunsEntry);
            // set the owning side to null (unless already changed)
            if ($importRunsEntry->getImportRun() === $this) {
                $importRunsEntry->setImportRun(null);
            }
        }

        return $this;
    }

    public function getRowsExpectedJson(): ?string
    {
        return $this->rows_expected_json;
    }

    public function setRowsExpectedJson(?string $rows_expected_json): self
    {
        $this->rows_expected_json = $rows_expected_json;

        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(?string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }
}
