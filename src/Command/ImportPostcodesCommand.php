<?php
// src/Command/ImportPostcodesCommand.php
namespace App\Command;

use App\Entity\ImportRuns;
use App\Entity\ImportRunsEntries;
use App\Entity\PostcodeEntries;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Console\Helper\ProgressBar;
use ZipArchive;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use App\Service\ImportRunsService;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;



class ImportPostcodesCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:importPostcodes';
    private $section1;
    private $section2;
    private $section3;
    private $progressBar;
    private $filesystem;
    private $importRun;
    private $progressConfirmation = false;
    private $irs;
    private $serializer;

    public function __construct(ImportRunsService $irs)
    {
        // Import Runs service
        $this->importRun = new ImportRuns();
        $this->irs = $irs;

        // Encoder for CSV files
        $encoders = [new CsvEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);

        parent::__construct();
    }
    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('This function will import UK postcode data from http://parlvid.mysociety.org/os/ into local database')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This function will import UK postcode data from http://parlvid.mysociety.org/os/ into local database. On success, the old entries will be removed.')
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // Setup the output sections
        $this->section1 = $output->section();
        $this->section2 = $output->section();
        $this->section3 = $output->section();

        // Local filesystem handle
        $this->filesystem = new Filesystem();
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        // Get confirmation from the user
        $this->section1->writeln([
            'You are about to import postcodes into database.',
            'This will refresh entries in your database',
        ]);
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action? (yes/no)', false, '/^yes/');

        $this->progressConfirmation = $helper->ask($input, $this->section1, $question);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->progressConfirmation) {
            // Start the process
            $this->initProgressBar($this->section2);
            $this->progressByStep('Preparing download');

            $this->importRun->setTimestamp(new \DateTime());
            $this->importRun->setErrorsCount(0);
            $this->irs->persistImportRun($this->importRun);
            $this->irs->flushChanges();
            $this->progressByStep();

            // Fetch the ZIP file
            $client = HttpClient::create();
            $response = $client->request('GET', 'http://parlvid.mysociety.org/os/code-point/codepo_gb-2020-05.zip');
            $this->progressByStep();

            if ($response->getStatusCode()==200 && $response->getHeaders()['content-type'][0]=='application/zip') {
                $this->progressByStep('Downloading data file...');

                // Store the ZIP file on local harddrive, in tmp dir
                $pathToTmp = DIRECTORY_SEPARATOR.trim(sys_get_temp_dir(),DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
                $zippedFileName = $pathToTmp.'postcodes.zip';
                $this->filesystem->dumpFile($zippedFileName, $response->getContent());
                $this->progressByStep('Processing data file...');

                // Unzip the file, still in tmp
                $zip = new ZipArchive;
                $unzippedFile = $zip->open($zippedFileName);
                $this->progressByStep();
                $unzippedTmpPath = $pathToTmp.'postcodes_tmp';
                if ($unzippedFile === TRUE) {
                    $this->progressByStep('Unzipping data file...');

                    // extract it to the path we determined above
                    $zip->extractTo($unzippedTmpPath);
                    $zip->close();
                    // Cleanup
                    $this->filesystem->remove($zippedFileName);
                    $this->progressByStep();
                } else {
                    // More cleanup
                    $this->filesystem->remove($zippedFileName);
                    $this->filesystem->remove($unzippedTmpPath);
                    return $this->finishWithFailure('Error processing zip file, import process aborted');
                }

                // Process the metadata file, to get file version and expected rows count for each postcode area
                $finder = new Finder();
                $finder->files()->in($unzippedTmpPath.DIRECTORY_SEPARATOR.'Doc')->name('metadata.txt');

                if ($finder->hasResults()){
                    $this->progressByStep('Processing metadata file...');
                    $files = iterator_to_array($finder);
                    if (isset($files[$unzippedTmpPath.DIRECTORY_SEPARATOR.'Doc'.DIRECTORY_SEPARATOR.'metadata.txt']) && is_object($files[$unzippedTmpPath.DIRECTORY_SEPARATOR.'Doc'.DIRECTORY_SEPARATOR.'metadata.txt'])) {
                        $file = $files[$unzippedTmpPath.DIRECTORY_SEPARATOR.'Doc'.DIRECTORY_SEPARATOR.'metadata.txt'];
                        $contents = $file->getContents();
                        $lines = preg_split("/\r\n|\n|\r/", $contents);

                        $importMetadata = '';
                        foreach ($lines as $number=>$line){
                            // first 5 lines contain metadata, so we handle these separately
                            if (intval($number<5)) {
                                $importMetadata .= $line."\n";
                            } elseif (!empty(trim($line))) {
                                // Parse each row with postcode area expected number of entries
                                $importDetailsArray = preg_split("/[\h]+/", $line);
                                $areaCode = trim($importDetailsArray[1]);
                                $expectedRows = intval($importDetailsArray[2]);

                                if(strlen($areaCode)==1 || strlen($areaCode)==2) {
                                    $ire = new ImportRunsEntries();
                                    $ire->setRowsExpected($expectedRows)->setAreaCode($areaCode)->setRowsProcessed(0)->setRowsErrored(0);
                                    $ire->setImportRun($this->importRun);
                                    $this->importRun->addImportRunsEntry($ire);
                                    $this->irs->persistImportRunsEntry($ire);
                                    $this->irs->flushChanges();
                                }
                            }
                        }
                        $this->importRun->setMetadata($importMetadata);
                        $this->progressByStep('Processing data files...');

                        // Get to actual postcode data importing
                        $csvHeader = '"Postcode","Positional_quality_indicator","Eastings","Northings","Country_code","NHS_regional_HA_code","NHS_HA_code","Admin_county_code","Admin_district_code","Admin_ward_code"'."\n";
                        foreach ($this->importRun->getImportRunsEntries() as $ire) {
                            $this->progressByStep('Processing postcode starting with ' . $ire->getAreaCode());

                            $finder = new Finder();
                            $finder->files()->in($unzippedTmpPath . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . 'CSV')->name(strtolower($ire->getAreaCode()) . '.csv');

                            if ($finder->hasResults()) {
                                $files = iterator_to_array($finder);
                                $file = array_shift($files);

                                $contents = $csvHeader.$file->getContents();
                                $postcodeData = $this->serializer->deserialize($contents, PostcodeEntries::class, 'csv');

// @ToDo: continue from here - handle deserialized data from file, upload to DB, check for errors and the number of processed entries






                            }
                        }
                    } else {
                        $this->filesystem->remove($unzippedTmpPath);
                        return $this->finishWithFailure('Error processing metadata file, import process aborted');
                    }
                } else {
                    $this->filesystem->remove($unzippedTmpPath);
                    return $this->finishWithFailure('Error processing data file, import process aborted');
                }
                $this->filesystem->remove($unzippedTmpPath);
            } else {
                return $this->finishWithFailure('Incorrect file downloaded from source, import process aborted');
            }

            return $this->finishWithSuccess();
        } else {
            return $this->finishWithFailure('Import process aborted');
        }
    }

    /*
     * Handle progress bar initialisation
     */
    private function initProgressBar ($section)
    {
        $this->progressBar = new ProgressBar($section, 200);
        $this->progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s%');
        $this->progressBar->start();
    }

    /*
     * Progress the bar by 1 step, optional message to display below
     */
    private function progressByStep($msg='')
    {
        if (!empty($msg)) {
            $this->section3->clear();
            $this->section3->write($msg);
        }
        $this->progressBar->advance();
    }

    /*
     * Cleanup after successful run
     */
    private function finishWithSuccess($successMessage='')
    {
        if (!empty($successMessage)){
            $this->section1->write($successMessage);
        }
        $this->section3->clear();
        if (!empty($this->progressBar)) {
            $this->progressBar->finish();
        }
        $this->irs->persistImportRun($this->importRun);
        $this->irs->flushChanges();

        return Command::SUCCESS;
    }

    /*
     * Cleanup after unsuccessful run
     */
    private function finishWithFailure($errorMessage='')
    {
        if (!empty($errorMessage)){
            $this->section1->write($errorMessage);
            $this->importRun->addToErrorsString($errorMessage);
        }
        $this->section3->clear();
        if (!empty($this->progressBar)) {
            $this->progressBar->finish();
        }
        $this->importRun->incrementErrorsCount();
        $this->irs->persistImportRun($this->importRun);
        $this->irs->flushChanges();

        return Command::FAILURE;
    }
}