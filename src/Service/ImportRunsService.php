<?php
// src/Service/ImportRunsService.php
namespace App\Service;

use App\Entity\ImportRuns;
use App\Entity\ImportRunsEntries;
use Doctrine\ORM\EntityManagerInterface;

class ImportRunsService
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function persistImportRun(ImportRuns $ir)
    {
        $this->em->persist($ir);
    }

    public function persistImportRunsEntry(ImportRunsEntries $ire)
    {
        $this->em->persist($ire);
    }

    public function flushChanges()
    {
        $this->em->flush();
    }
}