<?php

namespace App\Repository;

use App\Entity\ImportRunsEntries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImportRunsEntries|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImportRunsEntries|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImportRunsEntries[]    findAll()
 * @method ImportRunsEntries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImportRunsEntriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImportRunsEntries::class);
    }

    // /**
    //  * @return ImportRunsEntries[] Returns an array of ImportRunsEntries objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImportRunsEntries
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
