<?php

namespace App\Repository;

use App\Entity\PostcodeEntries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostcodeEntries|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostcodeEntries|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostcodeEntries[]    findAll()
 * @method PostcodeEntries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostcodeEntriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostcodeEntries::class);
    }

    // /**
    //  * @return PostcodeEntries[] Returns an array of PostcodeEntries objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PostcodeEntries
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
