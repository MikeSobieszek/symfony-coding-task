<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200622125509 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE import_runs (id INT AUTO_INCREMENT NOT NULL, timestamp DATETIME NOT NULL, errors_count INT NOT NULL, errors_content LONGTEXT DEFAULT NULL, rows_expected_json LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE import_runs_entries (id INT AUTO_INCREMENT NOT NULL, import_run_id INT NOT NULL, rows_expected INT NOT NULL, rows_processed INT NOT NULL, rows_errored INT NOT NULL, errors_content LONGTEXT DEFAULT NULL, INDEX IDX_A6CDFF04F8D244DC (import_run_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE postcode_entries (id INT AUTO_INCREMENT NOT NULL, postcode VARCHAR(7) NOT NULL, positional_quality_indicator INT NOT NULL, eastings INT NOT NULL, northings INT NOT NULL, country_code VARCHAR(10) DEFAULT NULL, nhs_regional_ha_code VARCHAR(10) DEFAULT NULL, nhs_ha_code VARCHAR(10) DEFAULT NULL, admin_county_code VARCHAR(10) DEFAULT NULL, admin_district_code VARCHAR(10) DEFAULT NULL, admin_ward_code VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE import_runs_entries ADD CONSTRAINT FK_A6CDFF04F8D244DC FOREIGN KEY (import_run_id) REFERENCES import_runs (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE import_runs_entries DROP FOREIGN KEY FK_A6CDFF04F8D244DC');
        $this->addSql('DROP TABLE import_runs');
        $this->addSql('DROP TABLE import_runs_entries');
        $this->addSql('DROP TABLE postcode_entries');
    }
}
